#include <SoftwareSerial.h>

SoftwareSerial BT_Serial(0,1); /// RX / TX

/* PINS * (PIN_ANALOG, value_speed, PIN_DIGITAL, value_direction 0/1 ) */
int pins[3][4] = {
  /* LEFT */   {11, 0, 12, 1},
  /* MIDDLE */ {9, 0, 8, 1},
  /* RIGHT */ {6, 0, 7, 1},
};
/* POINTERS */
int A_LEFT_GEAR = 0, A_MIDDLE_GEAR = 1, A_RIGHT_GEAR = 2;
int A_PIN_ANALOG = 0, A_SPEED = 1, A_PIN_DIGITAL = 2, A_DIRECTION = 3;

int APP_BYTE = 94;

boolean run;

void setup() {
  Serial.begin(9600);
  Serial.println("Hello"); 

  BT_Serial.begin(9600);
  BT_Serial.write("AT+NAMEIO");
}

void loop() {
     while (Serial.available() > 0) {
      
        // SENDING 4 BYTES - APP_BYTE, LEFT, MIDDLE, RIGHT <0; 127 ; 255>, then it is doubled
        char speed;
        bool run = false;
        delay(50);
          
        for (int readByte = 0; readByte < 4; readByte++) {
            int realspeed = (int)Serial.read();
            int GEAR = readByte - 1;

            // It is our APP? Then reads next 3 bytes
            if (readByte == 0 && realspeed == APP_BYTE){
              run = true;
            } else {
              if (run){
                   if (realspeed >= 0 && realspeed <= 127){
                    // CORRECT AND SET DIRECTION
                    if (pins[GEAR][A_DIRECTION] == 0){
                        pins[GEAR][A_DIRECTION] = 1;
                        writeDirection(GEAR);
                      }
                  }
                  
                  if (realspeed > 127 && realspeed < 255){
                      // CORRECT AND SET DIRECTION
                      if (pins[GEAR][A_DIRECTION] == 1){
                        pins[GEAR][A_DIRECTION] = 0;
                        writeDirection(GEAR);
                      }
                      // CORRECT SPEED
                      realspeed = realspeed - 127;
                  }
      
                  // SET SPEED
                  if (pins[GEAR][A_SPEED] != realspeed && realspeed >= 0 && realspeed < 255){
                    pins[GEAR][A_SPEED] = realspeed * 2; // double speed, because max speed is 255 both direction
                    writeSpeed(GEAR);
                  }   
                }
            }
        }
    }    
}

void writeSpeed(int GEAR){
   analogWrite(pins[GEAR][A_PIN_ANALOG], pins[GEAR][A_SPEED]);
}

void writeDirection(int GEAR){
    digitalWrite(pins[GEAR][A_PIN_DIGITAL], (pins[GEAR][A_DIRECTION] == 0) ? LOW : HIGH);
}
