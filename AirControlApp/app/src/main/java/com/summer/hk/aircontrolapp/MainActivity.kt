package com.summer.hk.aircontrolapp

import android.bluetooth.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import io.github.controlwear.virtual.joystick.android.JoystickView
import android.content.Intent
import android.graphics.Color
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import java.util.*


class MainActivity : AppCompatActivity() {

    val SERVICE_UUID: UUID = UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb")
    val CHAR_UUID: UUID = UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb")
    val APP_BYTE = 94

    // TODO: to SpeedControl.kt
    val SPEED_DEFAULT: Int = 127 + 50
    val SPEED_MAX_CHANGE: Int = 127
    var speeds: IntArray = intArrayOf(0, 0, 0)
    var saveSpeeds: IntArray = intArrayOf(0, 0, 0)
    var started: Boolean = false

    var state = BluetoothProfile.STATE_DISCONNECTED
    var service: BluetoothGattService? = null
    var characteristics: BluetoothGattCharacteristic? = null
    var gatt: BluetoothGatt? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        if (!mBluetoothAdapter.isEnabled) {
            startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 1)
        }

        findViewById<Button>(R.id.buttonSetMemory).setOnClickListener{ view ->
            findViewById<Button>(R.id.buttonLoadMemory).text = "Load Memory ("+speeds[0]+","+speeds[1]+"," + speeds[2]+")"
            saveSpeeds = speeds
        }

        findViewById<Button>(R.id.buttonLoadMemory).setOnClickListener{ view ->
            speeds = saveSpeeds
            findViewById<SeekBar>(R.id.seekBarHeight).progress = Math.round((speeds[1] / 255f) * 100)
            if (speeds[0] == speeds[2]){
                findViewById<SeekBar>(R.id.seekBarSpeed).progress = Math.round((speeds[0]/ 255f) * 100)
            }
            synBLE()
        }

        findViewById<Button>(R.id.buttonConnection).setOnClickListener{ view ->
            if(mBluetoothAdapter.isEnabled) {
                val device = mBluetoothAdapter.getRemoteDevice("A4:D5:78:13:3B:05")
                device.connectGatt(this, false, BLECallBack)
            }
        }

        val starter: Button = findViewById<Button>(R.id.buttonStarter)

        findViewById<Button>(R.id.buttonStarter).setOnClickListener{ view ->
            if (state == BluetoothProfile.STATE_CONNECTED) {
                started = !started
                if (started){
                    starter.text = "STARTED"
                    if (speeds[0] == 0 && speeds[1] == 0 && speeds[2] == 0){
                        defaultSpeedAll()
                    } else {
                        synBLE()
                    }
                } else {
                    starter.text = "STOPPED"
                    started = !started
                    synBLE(intArrayOf(0, 0, 0))
                    started = !started
                }
            }
        }

        findViewById<Button>(R.id.buttonReset).setOnClickListener{ view ->

            if (state == BluetoothProfile.STATE_CONNECTED && gatt != null) {
                findViewById<SeekBar>(R.id.seekBarHeight).progress = Math.round((SPEED_DEFAULT / 255f) * 100)
                findViewById<SeekBar>(R.id.seekBarSpeed).progress = Math.round((SPEED_DEFAULT / 255f) * 100)
                findViewById<SeekBar>(R.id.seekBarLeftRight).progress = 50
                defaultSpeedAll()
            }
        }


        var speed = 0
        findViewById<SeekBar>(R.id.seekBarSpeed).setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, i: Int, fromUser: Boolean) {
                if (i != speed && fromUser){
                    speed = i

                    val realspeed = realSeekSpeed(speed)
                    setSpeeds(realspeed, null, realspeed)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }
        })

        /* SEEKBAR - MAX 100, speed 127 */
        var height = 0
        findViewById<SeekBar>(R.id.seekBarHeight).setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, i: Int, fromUser: Boolean) {
                if (i != height && fromUser){
                    height = i

                    setSpeeds(null, realSeekSpeed(height), null)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }
        })


        var leftRight = 0
        findViewById<SeekBar>(R.id.seekBarLeftRight).setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, fromUser: Boolean) {

                if (i != leftRight && fromUser){
                    leftRight = i

                    val realspeed = Math.round((Math.abs(50 - leftRight) * 2) / 100f * SPEED_MAX_CHANGE)
                    if (leftRight > 50){
                        setSpeeds(127 + realspeed, null, realspeed)
                    } else {
                        setSpeeds(realspeed, null, 127 + realspeed)
                    }
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }
        })
//
//        val joystick = findViewById<JoystickView>(R.id.joystickView)
//        var oldAngle = 0
//        var oldStrength = 0
//        var threadFree: Boolean = true
//        joystick.setOnMoveListener(
//                { angle, strength ->
////                    if (state == BluetoothProfile.STATE_CONNECTED) {
//                        // 5% of difference at least
//                        if (strength < 30){
//                            defaultSpeedAll()
//                        } else {
//                                if(threadFree){
//                                    threadFree = false
//
//                                    var speed = SPEED_MAX_CHANGE * (strength / 100)
//                                    if (angle < 180 && angle > 0){
//                                        setSpeeds(127 + speed, null, speed)
//                                    }
//                                    if (angle >= 180 && angle < 360){
//                                        setSpeeds(speed, null, speed + 127)
//                                    }
//                                threadFree = true
//                            }
//                        }
////                    }
//                },
//                1000
//        )
    }

    /* from 0 to 255 through 127, 0 and 255 are 100%, 127 is 0%  */
    fun realSeekSpeed(seekValue: Int): Int{

        var realspeed = 0

        if (seekValue >= 50){
            val scale = ((seekValue - 50f) / 50) * 100
            realspeed = Math.round(127 + (scale * 1.27f))

        } else {
            val scale = (((50 - seekValue) / 50f)) * 100
            realspeed = Math.round(scale * 1.27f)
        }

        return realspeed;
    }

    val BLECallBack = object : BluetoothGattCallback() {
        override fun onServicesDiscovered(newGatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(newGatt, status)

            gatt = newGatt

            service = newGatt?.getService(SERVICE_UUID)
            characteristics = newGatt?.getService(SERVICE_UUID)?.getCharacteristic(CHAR_UUID)

            zeroSpeedAll()
        }

        override fun onConnectionStateChange(newGatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(newGatt, status, newState)

            if (state != newState){
                state = newState

                val connectionButton = findViewById<Button>(R.id.buttonConnection)
//                connectionButton.setBackgroundColor(Color.RED)

                if (state == BluetoothProfile.STATE_CONNECTED) {
                    if (newGatt != null) {
                        gatt = newGatt
                        gatt?.discoverServices()
                        connectionButton.setText("CONNECTED")
                    }
                }

                if (state == BluetoothProfile.STATE_DISCONNECTED){
                    connectionButton.setText("DISCONNECTED")
                    connectionButton.setBackgroundColor(Color.RED)
                }
            }
        }
    }

    fun setSpeeds(change: Int){
        speeds = intArrayOf(speeds[0] + change, speeds[1] + change, speeds[2] + change)
        synBLE()
    }

    fun setSpeeds(left: Int?, middle: Int?, right: Int?){
        speeds = intArrayOf(left ?: speeds[0], middle ?: speeds[1] , right ?: speeds[2])
        synBLE()
    }

    fun defaultSpeedAll(){
        speeds = intArrayOf(SPEED_DEFAULT, SPEED_DEFAULT, SPEED_DEFAULT)
        synBLE()
    }

    fun zeroSpeedAll(){
        speeds = intArrayOf(0, 0, 0)
        synBLE()
    }

    fun synBLE(){
        synBLE(null)
    }

    fun synBLE(speeds: IntArray?){
        var toByte: IntArray = intArrayOf(APP_BYTE, 0, 0, 0)
        for (i in 1..3){
            toByte.set(i, speeds?.get(i-1)?: this.speeds.get(i-1))
            toByte.set(i, speeds?.get(i-1)?: this.speeds.get(i-1))
        }

        findViewById<TextView>(R.id.LeftSpeed).text = "Left:" + percentageSpeed(toByte[1])
        findViewById<TextView>(R.id.MiddleSpeed).text = "Middle:" + percentageSpeed(toByte[2])
        findViewById<TextView>(R.id.RightSpeed).text = "Right:" + percentageSpeed(toByte[3])

            if (started){
            characteristics?.setValue(toByte.foldIndexed(ByteArray(toByte.size)) { i, a, v -> a.apply { set(i, v.toByte()) } })
                gatt?.writeCharacteristic(characteristics)
        }
    }

    fun percentageSpeed (speed: Int): String{
        var realspeed = speed
        var result = " "
        if (realspeed > 127){
            realspeed -= 127
        } else {
            result += "-"
        }

        return result + Math.round((realspeed / 127f * 100)).toString()  + "%"
    }
}