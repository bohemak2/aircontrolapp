#include <SoftwareSerial.h>

SoftwareSerial BT_Serial(0,1); /// RX / TX

/* DEFAULTS */
int SPEED = 50;
int SPEED_CHANGE = 2.5;

/* PINS * (PIN_ANALOG, value_speed, PIN_DIGITAL, value_direction 0/1 ) */
int pins[3][4] = {
  /* LEFT */   {11, SPEED, 12, 1},
  /* MIDDLE */ {9, SPEED, 8, 1},
  /* RIGHT */ {6, SPEED, 7, 1},
};
/* POINTERS */
int A_LEFT_GEAR = 0, A_MIDDLE_GEAR = 1, A_RIGHT_GEAR = 2;
int A_PIN_ANALOG = 0, A_SPEED = 1, A_PIN_DIGITAL = 2, A_DIRECTION = 3;

boolean run;

void setup() {
  Serial.begin(9600); 
  Serial.println("Hello"); 

  BT_Serial.begin(9600);
  BT_Serial.write("AT+NAMEIO");
}

void loop() {
  
    boolean execute = (BT_Serial.available() > 0 || Serial.available() > 0);
    char data;

    if (Serial.available() > 0 || BT_Serial.available() > 0){
        data = BT_Serial.read();

      if(sizeof(data) > 0){
        
          /* RUN/STOP ALL GEARS */
          if (data == 'r'){
            run = !run;
            
            for (int i = 0; i < 3; i++){
              analogWrite(pins[i][A_PIN_ANALOG], (run ? pins[i][A_SPEED] : 0));
            }
            
            Serial.println(run? "run" : "stop"); 
          }

          /* DEFAULT SPEED ALL GEARS */
          if (data == 't'){            
            for (int i = 0; i < 3; i++){
              setSpeed(i, SPEED);
              setSpeed(i, LOW);
            }
            
            Serial.println("restart"); 
          }
  
          /* LEFT GEAR */
          if (data == 'q'){
            setSpeed(A_LEFT_GEAR, true);
          }
          if (data == 'a'){
            setDirection(A_LEFT_GEAR);
          }
          if (data == 'z'){
            setSpeed(A_LEFT_GEAR, false);
          }
  
          /* BOTTOM GEARS */
          if (data == 'w'){
            setSpeed(A_MIDDLE_GEAR, true);
          }
          if (data == 's'){
            setDirection(A_MIDDLE_GEAR);
          }
          if (data == 'x'){
            setSpeed(A_MIDDLE_GEAR, false);
          }
  
          /* RIGHT GEAR */
          if (data == 'e'){
            setSpeed(A_RIGHT_GEAR, true);
          }
          if (data == 'd'){
            setDirection(A_RIGHT_GEAR);
          }
          if (data == 'c'){
            setSpeed(A_RIGHT_GEAR, false);
          }       
        }
    }    
}

void setSpeed(int GEAR, int value){
  
  if (value >= 0 && value <= 255){
    pins[GEAR][A_SPEED] = SPEED;
  }

  if (run){
    analogWrite(pins[GEAR][A_PIN_ANALOG], pins[GEAR][A_SPEED]); 
  }
}

void setSpeed(int GEAR, boolean speed_up){
  
  if (speed_up && pins[GEAR][A_SPEED] + SPEED_CHANGE < 255){
    
    pins[GEAR][A_SPEED] += SPEED_CHANGE;
    
  } else if(!speed_up && pins[GEAR][A_SPEED] - SPEED_CHANGE >= 0) {
    
    pins[GEAR][A_SPEED] -= SPEED_CHANGE;
    
  }

  if (run){
    analogWrite(pins[GEAR][A_PIN_ANALOG], pins[GEAR][A_SPEED]); 
  }
}

void setDirection(int GEAR){
  setDirection(GEAR, pins[GEAR][A_DIRECTION] == 1 ? 0 : 1);  
}

void setDirection(int GEAR, boolean direction){
  pins[GEAR][A_DIRECTION] = direction;  

  if (run){
    digitalWrite(pins[GEAR][A_PIN_DIGITAL], (pins[GEAR][A_DIRECTION] == 0) ? LOW : HIGH);
  }
}
