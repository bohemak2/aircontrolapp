/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.bluetoothlegatt;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Layout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import io.github.controlwear.virtual.joystick.android.JoystickView;

import static java.lang.Math.floor;
import static java.lang.Math.round;

/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControlActivity extends Activity {
    private final static String TAG = DeviceControlActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

    //====================================================================================================================================================
    private static SeekBar seek_bar;

    private static Button changecontrol;
    int controlflag = 1;
    private static LinearLayout buttoncontrol;
    private static LinearLayout joystickcontrol;


    private static LinearLayout settingcontrol;
    private static SeekBar maxbar, forbar, backbar;
    private static TextView maxtext, fortext, backtext;
    int maxspeed = 255;
    String forspeed = "100", backspeed = "150";


    private static TextView text_view;
    private static TextView outputtext;
    private static Button forward,backward,left,right,reset;

    String l_direction = "1", m_direction = "1", r_direction = "1";
    String l_pwm = "000", m_pwm = "000", r_pwm = "000";
    String output;
    //====================================================================================================================================================

    private TextView mConnectionState;
    private TextView mDataField;
    private String mDeviceName;
    private String mDeviceAddress;
    private ExpandableListView mGattServicesList;
    private BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;


    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";

    //====================================================================================================================================================
    public void setting(){
        maxbar = (SeekBar)  findViewById(R.id.max_bar);
        forbar = (SeekBar)  findViewById(R.id.for_bar);
        backbar = (SeekBar)  findViewById(R.id.back_bar);
        maxtext = (TextView)  findViewById(R.id.max_text);
        fortext = (TextView)  findViewById(R.id.for_text);
        backtext = (TextView)  findViewById(R.id.back_text);

        maxbar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        maxspeed = progress;
                        maxtext.setText("Max. Speed: " + maxspeed);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {


                    }
                }
        );

        forbar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        forspeed = Integer.toString(progress);
                        if (forspeed.length() == 1){
                            forspeed = "00" + forspeed;
                        }
                        else if (forspeed.length() == 2){
                            forspeed = "0" + forspeed;
                        }
                        fortext.setText("Forward Speed: " + forspeed);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {


                    }
                }
        );

        backbar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        backspeed = Integer.toString(progress);
                        if (backspeed.length() == 1){
                            backspeed = "00" + backspeed;
                        }
                        else if (backspeed.length() == 2){
                            backspeed = "0" + backspeed;
                        }
                        backtext.setText("Backward Speed: " + backspeed);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {


                    }
                }
        );
    }

    public void send(){
        outputtext = (TextView) findViewById(R.id.outputtext);
        output= String.join("",l_direction,l_pwm,m_direction,m_pwm,r_direction,r_pwm);
        outputtext.setText("Output: " + output);
        mBluetoothLeService.ouptputvalue(output);
    }

    public void seekbar() {
        seek_bar = (SeekBar) findViewById(R.id.seekBar);
        text_view = (TextView) findViewById(R.id.mstring);
        text_view.setText("                    Middle: 1000");

        seek_bar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                        if (progress < 256){
                            m_direction = "0";
                            m_pwm = Integer.toString(255 - progress);
                            if (m_pwm.length() == 1){
                                m_pwm = "00" + m_pwm;
                            }
                            else if (m_pwm.length() == 2){
                                m_pwm = "0" + m_pwm;
                            }
                        }
                        else{
                            m_direction = "1";
                            m_pwm = Integer.toString(progress - 256);
                            if (m_pwm.length() == 1){
                                m_pwm = "00" + m_pwm;
                            }
                            else if (m_pwm.length() == 2){
                                m_pwm = "0" + m_pwm;
                            }
                        }

                        text_view.setText("                    Middle: " + m_direction + m_pwm);
                        send();
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {}
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        send();
                    }
                }
        );
    }
    //====================================================================================================================================================


    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
                clearUI();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                displayGattServices(mBluetoothLeService.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };

    // If a given GATT characteristic is selected, check for supported features.  This sample
    // demonstrates 'Read' and 'Notify' features.  See
    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
    // list of supported characteristic features.
    private final ExpandableListView.OnChildClickListener servicesListClickListner =
            new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                            int childPosition, long id) {
                    if (mGattCharacteristics != null) {
                        final BluetoothGattCharacteristic characteristic =
                                mGattCharacteristics.get(groupPosition).get(childPosition);
                        final int charaProp = characteristic.getProperties();
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
                            if (mNotifyCharacteristic != null) {
                                mBluetoothLeService.setCharacteristicNotification(
                                        mNotifyCharacteristic, false);
                                mNotifyCharacteristic = null;
                            }
                            mBluetoothLeService.readCharacteristic(characteristic);
                        }
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                            mNotifyCharacteristic = characteristic;
                            mBluetoothLeService.setCharacteristicNotification(
                                    characteristic, true);
                        }
                        return true;
                    }
                    return false;
                }
    };

    private void clearUI() {
        mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
        mDataField.setText(R.string.no_data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gatt_services_characteristics);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        // Sets up UI references.
        ((TextView) findViewById(R.id.device_address)).setText(mDeviceAddress);
        mGattServicesList = (ExpandableListView) findViewById(R.id.gatt_services_list);
        mGattServicesList.setOnChildClickListener(servicesListClickListner);
        mConnectionState = (TextView) findViewById(R.id.connection_state);
        mDataField = (TextView) findViewById(R.id.data_value);

        getActionBar().setTitle(mDeviceName);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        //====================================================================================================================================================
        seekbar();
        setting();

        changecontrol = (Button) findViewById(R.id.changecontrol);
        changecontrol.setText("Button Control");
        buttoncontrol = (LinearLayout) findViewById(R.id.button_control);
        joystickcontrol = (LinearLayout) findViewById(R.id.joystick_control);
        settingcontrol = (LinearLayout) findViewById(R.id.setting_control);

        reset = (Button)    findViewById(R.id.reset);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seek_bar.setProgress(256);
                l_direction = "1";
                m_direction = "1";
                r_direction = "1";
                l_pwm = "000";
                m_pwm = "000";
                r_pwm = "000";
                text_view.setText("                    Middle: 1000");
                send();
            }
        });
        reset.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });

        changecontrol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(controlflag==1){
                    changecontrol.setText("Setting");
                    buttoncontrol.setVisibility(View.VISIBLE);
                    joystickcontrol.setVisibility(View.GONE);
                    settingcontrol.setVisibility(View.GONE);
                    controlflag = 2;
                }
                else if (controlflag == 2){
                    changecontrol.setText("Joystick Control");
                    buttoncontrol.setVisibility(View.GONE);
                    joystickcontrol.setVisibility(View.GONE);
                    settingcontrol.setVisibility(View.VISIBLE);
                    controlflag = 3;
                }else{
                    changecontrol.setText("Button Control");
                    buttoncontrol.setVisibility(View.GONE);
                    joystickcontrol.setVisibility(View.VISIBLE);
                    settingcontrol.setVisibility(View.GONE);
                    controlflag = 1;
                }

            }
        });

        JoystickView joystick = (JoystickView)  findViewById(R.id.joystick);
        joystick.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {
                double dpwm = (strength - 20)*3.1875;
                if (dpwm < 0){
                    dpwm = 0;
                }else if (dpwm >maxspeed){
                    dpwm = maxspeed;
                }
                int pwm = (int) dpwm;

                if (angle < 20 || angle > 340){
                    l_direction = "1";
                    r_direction = "1";
                    l_pwm = Integer.toString(pwm);
                    r_pwm = Integer.toString(pwm);
                }else if (angle > 160 && angle < 200){
                    l_direction = "0";
                    r_direction = "0";
                    l_pwm = Integer.toString(pwm);
                    r_pwm = Integer.toString(pwm);
                }else if (angle > 70 && angle <= 110){
                    l_direction = "0";
                    r_direction = "1";
                    r_pwm = Integer.toString(pwm);
                    l_pwm = Integer.toString(pwm);
                    //double dangle = angle - 30;
                    //dpwm = dangle/120*dpwm;
                    //pwm = (int) dpwm;
                }else if (angle > 250 && angle < 290){
                    l_direction = "1";
                    r_direction = "0";
                    l_pwm = Integer.toString(pwm);
                    r_pwm = Integer.toString(pwm);
                    //double dangle = 330 - angle;
                    //dpwm = dangle/120*dpwm;
                    //pwm = (int) dpwm;
                }

                else if (angle >= 20 && angle <= 70){
                    l_direction = "1";
                    r_direction = "1";
                    r_pwm = Integer.toString(pwm);
                    double dangle = 70 - angle;
                    dpwm = dangle/50*dpwm;
                    pwm = (int) dpwm;
                    l_pwm = Integer.toString(pwm);
                }else if (angle >= 290 && angle <= 340){
                    l_direction = "1";
                    r_direction = "1";
                    l_pwm = Integer.toString(pwm);
                    double dangle = angle - 290;
                    dpwm = dangle/50*dpwm;
                    pwm = (int) dpwm;
                    r_pwm = Integer.toString(pwm);
                }else if (angle >= 110 && angle <= 160){
                    l_direction = "0";
                    r_direction = "0";
                    r_pwm = Integer.toString(pwm);
                    double dangle = angle - 110;
                    dpwm = dangle/50*dpwm;
                    pwm = (int) dpwm;
                    l_pwm = Integer.toString(pwm);
                }else if (angle >= 200 && angle <= 250){
                    l_direction = "0";
                    r_direction = "0";
                    l_pwm = Integer.toString(pwm);
                    double dangle = 250 - angle;
                    dpwm = dangle/50*dpwm;
                    pwm = (int) dpwm;
                    r_pwm = Integer.toString(pwm);
                }

                //text_view.setText("angle: " + angle + " dpwm: " + dpwm);



                if (l_pwm.length() == 1){
                    l_pwm = "00" + l_pwm;
                }
                else if (l_pwm.length() == 2){
                    l_pwm = "0" + l_pwm;
                }

                if (r_pwm.length() == 1){
                    r_pwm = "00" + r_pwm;
                }
                else if (r_pwm.length() == 2){
                    r_pwm = "0" + r_pwm;
                }
                send();
            }
        });

        left = (Button) findViewById(R.id.button_left);

        left.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN ) {
                    l_direction = "0";
                    r_direction = "1";
                    l_pwm = "255";
                    r_pwm = "255";
                    send();
                    return true;
                }
                else if(event.getAction() == MotionEvent.ACTION_UP ){
                    l_direction = "1";
                    r_direction = "1";
                    l_pwm = "000";
                    r_pwm = "000";
                    send();
                    return true;
                }
                return false;
            }
        });

        right = (Button) findViewById(R.id.button_right);
        right.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN ) {
                    l_direction = "1";
                    r_direction = "0";
                    l_pwm = "255";
                    r_pwm = "255";
                    send();
                    return true;
                }
                else if(event.getAction() == MotionEvent.ACTION_UP ){
                    l_direction = "1";
                    r_direction = "1";
                    l_pwm = "000";
                    r_pwm = "000";
                    send();
                    return true;
                }
                return false;
            }
        });


        forward = (Button) findViewById(R.id.button_forward);
        forward.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN ) {
                    l_direction = "1";
                    r_direction = "1";
                    l_pwm = forspeed;
                    r_pwm = forspeed;
                    send();
                    return true;
                }
                else if(event.getAction() == MotionEvent.ACTION_UP ){
                    l_direction = "1";
                    r_direction = "1";
                    l_pwm = "000";
                    r_pwm = "000";
                    send();
                    return true;
                }
                return false;
            }
        });

        backward = (Button) findViewById(R.id.button_backward);
        backward.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN ) {
                    l_direction = "0";
                    r_direction = "0";
                    l_pwm = backspeed;
                    r_pwm = backspeed;
                    send();
                    return true;
                }
                else if(event.getAction() == MotionEvent.ACTION_UP ){
                    l_direction = "1";
                    r_direction = "1";
                    l_pwm = "000";
                    r_pwm = "000";
                    send();
                    return true;
                }
                return false;
            }
        });
        //=================================================================================================================================================

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_connect:
                mBluetoothLeService.connect(mDeviceAddress);
                return true;
            case R.id.menu_disconnect:
                mBluetoothLeService.disconnect();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectionState.setText(resourceId);
            }
        });
    }

    private void displayData(String data) {
        if (data != null) {
            mDataField.setText(data);
        }
    }

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            currentServiceData.put(
                    LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                currentCharaData.put(
                        LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }

        SimpleExpandableListAdapter gattServiceAdapter = new SimpleExpandableListAdapter(
                this,
                gattServiceData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_UUID},
                new int[] { android.R.id.text1, android.R.id.text2 },
                gattCharacteristicData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_UUID},
                new int[] { android.R.id.text1, android.R.id.text2 }
        );
        mGattServicesList.setAdapter(gattServiceAdapter);
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }
}
